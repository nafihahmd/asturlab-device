## Features Added
- Trasmit amd Receive LoRa messages
- BLE
- Display RSSI & Temperature
- Power on, off
- GPS
- Battery level
- Option to set 
	- Bandwidth
	- Spread factor
	- Coding rate
	- Tx power

## To-Do
- Mesh network
- Private channels
- Encryption
- Uplink and Downlink support
- Audio Support



